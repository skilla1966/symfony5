module.exports = {
    moduleFileExtensions: [
        "js",
        "json",
        "jsx",
        "ts",
        "tsx",
        "json",
        "es"
    ],
    transform: {
        '^.+\\.(js|jsx|es)?$': 'babel-jest'
    },
    testEnvironment: 'node',
    moduleNameMapper: {
        '^root/(.*)$': '<rootDir>/srcjs/$1',
        '^page/(.*)$': '<rootDir>/srcjs/Pages/$1',
        '^component/(.*)$': '<rootDir>/srcjs/Components/$1',
        '^shared/(.*)$': '<rootDir>/srcjs/Shared/$1'
    },
    testMatch: [
        '<rootDir>/testsjs/**/*.(test|spec).(js|jsx|ts|tsx|es)'
    ],
    transformIgnorePatterns: ['<rootDir>/node_modules/']
};
