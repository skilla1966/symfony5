#!/bin/bash

#docker pull node:lts
#docker build -t nodeforsymfony:latest -f DockerfileNode .
#
#docker pull php:7.4-apache
#docker build -t phpforsymfony:latest -f DockerfilePhp .

rm -rf node_modules
rm -rf yarn.lock

echo "{ "                                                                                                                   > package.json
echo "    \"dependencies\": {"                                                                                             >> package.json
echo "    },"                                                                                                              >> package.json
echo "    \"name\": \"JavaScript\","                                                                                       >> package.json
echo "    \"version\": \"1.0.0\","                                                                                         >> package.json
echo "    \"main\": \"KataJavaScript\","                                                                                   >> package.json
echo "    \"directories\": {"                                                                                              >> package.json
echo "        \"test\": \"tests\""                                                                                         >> package.json
echo "    },"                                                                                                              >> package.json
echo "    \"devDependencies\": {},"                                                                                        >> package.json
echo "    \"scripts\": {"                                                                                                  >> package.json
echo "        \"build\": \"node_modules/.bin/webpack --mode=development --entry=./src/app.js --output=./dist/bundle.js\"," >> package.json
echo "        \"test\": \"jest\""                                                                                          >> package.json
echo "    },"                                                                                                              >> package.json
echo "    \"keywords\": [],"                                                                                               >> package.json
echo "    \"author\": \"\","                                                                                               >> package.json
echo "    \"license\": \"MIT\","                                                                                           >> package.json
echo "    \"description\": \"\","                                                                                          >> package.json
echo "    \"private\": true"                                                                                               >> package.json
echo "}"                                                                                                                   >> package.json

docker run -it --rm --name node4symfony -v "$PWD":/usr/src/app -w /usr/src/app nodeforsymfony yarn add \
    @babel/core \
    @babel/preset-env \
    @babel/preset-react \
    @babel/preset-typescript \
    @fortawesome/fontawesome-svg-core \
    @fortawesome/free-brands-svg-icons \
    @fortawesome/free-regular-svg-icons \
    @fortawesome/free-solid-svg-icons \
    @fortawesome/react-fontawesome \
    babel-loader \
    babel-plugin-module-resolver \
    bootstrap \
    css-loader \
    file-loader \
    html-webpack-plugin \
    jest \
    jquery \
    mini-css-extract-plugin \
    node-sass \
    popper.js \
    react \
    react-dom \
    react-redux \
    react-router-dom \
    redux \
    sass-loader \
    style-loader \
    ts-loader \
    typescript \
    webpack \
    webpack-cli \
    webpack-dev-server
