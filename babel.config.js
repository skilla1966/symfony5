module.exports = {
    presets: [
        [
            '@babel/preset-env',
            {
                targets: {
                    node: 'current'
                }
            }
        ],
        '@babel/preset-react',
        '@babel/preset-typescript',
    ],
    plugins: [
        ["module-resolver", {
            root: ["./srcjs"],
            alias: {
                root: "./srcjs",
                page: './srcjs/Pages',
                component: './srcjs/Components',
                shared: './srcjs/Shared',
                reducer: "./srcjs/Reducers",
                action: "./srcjs/Actions",
                constant: "./srcjs/Constants",
                html: './assets/Html',
                image: './assets/Images',
                test: "./testsjs",
            }
        }]
    ]
};
