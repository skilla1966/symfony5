const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');

module.exports = {
    resolve: {
        extensions: [
            '.js',
            '.es',
            '.ts',
            '.jsx',
            '.html'
        ],
        alias: {
            'root': path.resolve(__dirname, 'srcjs'),
            'page': path.resolve(__dirname, 'srcjs/Pages'),
            'component': path.resolve(__dirname, 'srcJs/Components'),
            'shared': path.resolve(__dirname, 'srcjs/Shared'),
            'reducer': path.resolve(__dirname, 'srcJs/Reducers'),
            'action': path.resolve(__dirname, 'srcJs/Actions'),
            'constant': path.resolve(__dirname, 'srcJs/Constants'),
            'html': path.resolve(__dirname, 'assets/Html'),
            'image': path.resolve(__dirname, 'assets/Images')
        }
    },
    entry: {
        app: 'root/App/App'
    },
    output: {
        path: path.resolve(__dirname, 'public', 'build'),
        filename: '[name].bundle.js',
        publicPath: "/build/"
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'assets', 'html', 'index.html'),
            filename: path.resolve(__dirname, 'public', 'build', 'index.html')
        }),
        new MiniCSSExtractPlugin({
            filename: '[name].css'
        })
    ],
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.(js|jsx|es)$/,
                exclude: /node_modules/,
                loader: "babel-loader",
            },
            {
                test: /\.(ts)$/,
                exclude: /node_modules/,
                loader: "ts-loader"
            },
            {
                test: /\.s[ac]ss$/,
                loader: [
                    process.env.NODE_ENV === 'production' ? "style-loader" : MiniCSSExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: process.env.NODE_ENV !== 'production'
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: process.env.NODE_ENV !== 'production'
                        }
                    }
                ]
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/,
                loader: "file-loader",
                options: {
                    outputPath:'images',
                    esModule: true
                }
            }
        ]
    },
    target: 'web',
    stats: {
        colors: true
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'public', 'build'),
        http2: false,
        compress: true,
        host: '0.0.0.0',
        port: 8888
    }
};
