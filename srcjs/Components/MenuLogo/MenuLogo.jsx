import React, { Component } from 'react';
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import Style from "./MenuLogo.scss";

/**
 * Función que coge datos de redux (state) y los parametros del componente (componentProps) y genera y retorna
 * las props adicionales que se pasan al componente (no hay que retornar componentProps de eso se encarga redux-connect)
 */
function mapStateToProps(state, componentProps) {
    return {
    }
}

/**
 * Funcion que mapea acciones y datos para facilitar el dispatch de eventos al store de redux
 * aún así retornamos tambien dispatch por compatibilidad
 */
function mapDispatchToProps(dispatch, componentProps) {
    return {
        dispatch, /** the default dispatcher */
        headerClicked: function() {dispatch()}
    }
}

class MenuLogo extends Component {

    handleMouseOver(event) {
        event.preventDefault();
    };

    handleMouseOut(event) {
        event.preventDefault();
    };

    render() {
        const classes = `menulogo ${this.props.status ? "active" : ""} ${this.props.menuElement.position}`;
        return (
            <li
                className={classes}
                onMouseOver={(event) => this.handleMouseOver(event)}
                onMouseOut={(event) => this.handleMouseOut(event)}
            >
                <Link to={this.props.menuElement.url}><img alt="logo" src={this.props.menuElement.path} />{this.props.menuElement.text}</Link>
            </li>
        )
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuLogo);
