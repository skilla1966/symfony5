import React, { Component } from 'react';
import MenuItem from "component/MenuItem/MenuItem";
import MenuLogo from "component/MenuLogo/MenuLogo";
import {connect} from "react-redux";
import Style from "./Menu.scss";

/**
 * Función que coge datos de redux (state) y los parametros del componente (componentProps) y genera y retorna
 * las props adicionales que se pasan al componente (no hay que retornar componentProps de eso se encarga redux-connect)
 */
function mapStateToProps(state, componentProps) {
    return {
        menu: state.menu,
        menuActive: componentProps.menuActive
    }
}

/**
 * Funcion que mapea acciones y datos para facilitar el dispatch de eventos al store de redux
 * aún así retornamos tambien dispatch por compatibilidad
 */
function mapDispatchToProps(dispatch, componentProps) {
    return {
        dispatch, /** the default dispatcher */
        headerClicked: function() {dispatch()}
    }
}

class Menu extends Component {

    render() {
        const menuElements = this.props.menu.elements;
        const menuActive = this.props.menuActive;
        return (
            <ul>
                {
                    Object.keys(menuElements).map(
                        function (key, index) {
                            if (menuElements[key].type === 'logo') {
                                return (
                                    <MenuLogo key={ index } menuElement={ menuElements[key] } status={ key===menuActive } menuName={ key } />
                                )
                            }
                            if (menuElements[key].type === 'link') {
                                return (
                                    <MenuItem key={ index } menuElement={ menuElements[key] } status={ key===menuActive } menuName={ key } />
                                )
                            }
                        }
                    )
                }
            </ul>
        )
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
