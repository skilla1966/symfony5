import logotipo from "image/logotipo.png";

const initialState = {
    menu: {
        elements: {
            logo:  {type: 'logo', text: 'TimeTrack<sub>pro</sub>', url: '#',      position: 'left',  path: logotipo, status: ''},
            home:  {type: 'link', text: 'Home',                    url: '/',      position: 'left',  path: '#',      status: ''},
            user:  {type: 'link', text: 'User',                    url: '/user',  position: 'left',  path: '#',      status: ''},
            about: {type: 'link', text: 'About',                   url: '/about', position: 'right', path: '#',      status: ''}
        }
    }
};

function rootReducer(state = initialState, action) {
    return state;
}

export default rootReducer;
