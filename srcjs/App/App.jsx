import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { faCheckSquare, faCoffee } from '@fortawesome/free-solid-svg-icons';
import { createStore } from "redux";
import rootReducer from "reducer/rootReducer";
import { Provider } from "react-redux";
import setActiveMenu from "action/setActiveMenu";
import Style from "root/App/App.scss";
import Home from 'page/Home/Home';
import About from 'page/About/About';
import User from 'page/User/User';

library.add(fab, faCheckSquare, faCoffee);

const store = createStore(rootReducer);

ReactDOM.render(
    <Provider store={store}>
        <Router>
            {/* A <Switch> looks through its children <Route>s and renders the first one that matches the current URL. */}
            <Switch>
                <Route path="/about">
                    <About />
                </Route>
                <Route path="/user">
                    <User />
                </Route>
                <Route path="/">
                    <Home />
                </Route>
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('wrapper')
);
