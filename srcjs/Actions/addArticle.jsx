import { ADD_ARTICLE } from 'constant/action-types'

export function addArticle(payload) {
    return { type: ADD_ARTICLE, payload};
};
