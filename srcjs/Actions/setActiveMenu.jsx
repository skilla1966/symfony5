import { SET_ACTIVE_MENU } from 'constant/action-types'

export function setActiveMenu(payload) {
    return { type: SET_ACTIVE_MENU, payload};
};
