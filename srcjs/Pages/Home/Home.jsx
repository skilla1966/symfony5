import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from 'shared/Header/Header';
import Style from "./Home.scss";

/**
 * Función que coge datos de redux (state) y los parametros del componente (componentProps) y genera y retorna
 * las props adicionales que se pasan al componente (no hay que retornar componentProps de eso se encarga redux-connect)
 */
function mapStateToProps(state, componentProps) {
    return {
    }
}

/**
 * Funcion que mapea acciones y datos para facilitar el dispatch de eventos al store de redux
 * aún así retornamos tambien dispatch por compatibilidad
 */
function mapDispatchToProps(dispatch, componentProps) {
    return {
        dispatch, /** the default dispatcher */
        headerClicked: function() {dispatch()}
    }
}

/**
 *
 */
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <>
                <Header menuActive={ "home" } />
                <h1>Home</h1>
            </>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
