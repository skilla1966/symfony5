import React from 'react';

import Style from "./Logo.scss";

class Logo extends React.Component {
    render() {
        return (
            <a className='logo' href={this.props.destinationUrl}><img alt="" src={this.props.imagePath} />&nbsp;{this.props.text}&nbsp;</a>
        )
    }
}

export default Logo;
