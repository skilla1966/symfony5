import React, { Component } from 'react';
import Menu from "component/Menu/Menu";
import {connect} from "react-redux";
import Style from "./Header.scss";

/**
 * Función que coge datos de redux (state) y los parametros del componente (componentProps) y genera y retorna
 * las props adicionales que se pasan al componente (no hay que retornar componentProps de eso se encarga redux-connect)
 */
function mapStateToProps(state, componentProps) {
    return {
        menuActive: componentProps.menuActive
    }
}

/**
 * Funcion que mapea acciones y datos para facilitar el dispatch de eventos al store de redux
 * aún así retornamos tambien dispatch por compatibilidad
 */
function mapDispatchToProps(dispatch, componentProps) {
    return {
        dispatch, /** the default dispatcher */
        headerClicked: function() {dispatch()}
    }
}

class Header extends Component {
    render() {
        return (
            <div className='header'>
                <nav>
                    <Menu menuActive={ this.props.menuActive } />
                </nav>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
