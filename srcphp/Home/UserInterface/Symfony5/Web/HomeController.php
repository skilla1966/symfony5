<?php

declare(strict_types=1);

namespace TimeTrack\Main\Home\UserInterface\Symfony5\Web;

use Symfony\Component\HttpFoundation\Response;

class HomeController
{
    public function __invoke()
    {
        return new Response(
            $this->content(),
            $this->status(),
            $this->headers()
        );
    }

    private function content(): string
    {
        $html = file_get_contents(__DIR__ . '/../../../../../public/build/index.html');
        //$html = str_replace('"app.', '"build/app.', $html);

        return $html;
    }

    private function status(): int
    {
        return 200;
    }

    private function headers(): array
    {
        return [];
    }
}
